/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exemplo5;

/**
 *
 * @author lwyz_
 */
public class BufferSincronizado implements Buffer{
    private int buffer = -1; //Compartilhado pelas threads producer e consumer
    
    public synchronized void set (int value ) throws InterruptedException{
        
        System.out.printf("Produtor escreve\t%2d", value);
        buffer = value;
    };
    
    public synchronized int get () throws InterruptedException{
        System.out.printf("Consumidor lê   \t%2d", buffer);
        return buffer;
    }
    
}
